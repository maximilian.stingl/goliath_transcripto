### les parametres

filename = "betacells/betacells_ordered.csv" # le nom du fichier sortant de GeneSpring
# la premi?re colonne doit forc?ment contenir les Agilent ProbeName
raw_cols = 2:80 # numeros des colonnes contenant les valeurs brutes
n_conditions = 15 # le nombre de conditions differentes
n_replicas = 5 # le nombre de replicas par condition
flag_cols = 81:159 # numeros des colonnes contenant les flags pour chaque condition
detection_limit = 0# le nombre minimum de flags "Detected" necessaires pour garder une ligne
output_name = "betacells/betacells_quantile_normalized_newtestingnoremoval.csv" # nom du fichier en sortie
notdetected_name = "betacells/betacells_notdetected.txt" # nom du fichier ou seront gard? les probes Non d?tect?es

# noms des nouvelles colonnes a creer, peut etre remplace par NULL pour avoir les noms par defaut de R
a = 'Vehicle	BPA 1 pM	BPA 1 nM	BPA 1 uM	TBT 20 nM	TBT 50 nM	TBT 200 nM	TCN 1 nM	TCN 1 uM	DDE 1 nM	DDE 1 uM	PFOA 10 pM	PFOA 1 nM	PFOA 1 uM	TPP 1 nM	TPP 1 uM'
b = strsplit(a, '\t')
b = b[[1]]
new_cols = {}
flag_colnames = {}
for(x in b){
  if(x == b[length(b)]){
    for(i in 1:3){
      new_cols = append(new_cols, paste(x, i))
      flag_colnames = append(flag_colnames, paste(x, i, 'Flag'))
    }
    new_cols = append(new_cols, paste(x, 5))
    flag_colnames = append(flag_colnames, paste(x, 5, 'Flag'))
  }
  else{
    for(i in 1:5){
      new_cols = append(new_cols, paste(x, i))
      flag_colnames = append(flag_colnames, paste(x, i, 'Flag'))
    }
  }
}

###

library(preprocessCore)
library(data.table)
library(dplyr)
library(matrixStats)

microarray_data = data.frame(fread(filename))
rawvals = microarray_data[raw_cols] #selectionne les colonnes contenant les valeurs d'intensite brutes

# Etape 1: Filtration par les flags

for (i in 1:n_conditions){
  if (i == n_conditions){
    temp = data.frame(microarray_data[unlist(flag_cols[(n_replicas*(i-1)+1):((n_replicas)*i-1)])])
  }
  else{
      temp = data.frame(microarray_data[unlist(flag_cols[(n_replicas*(i-1)+1):(n_replicas*i)])])
  }
  microarray_data[paste("compromised_", i, sep="")] = list(rowSums(temp=="Compromised")==0) 
  microarray_data[paste("detected_", i, sep="")] = list(rowSums(temp=="Detected")>=detection_limit) 
  
} # pour chaque ligne dans chaque condition, decide si la ligne est gardee ou non

notcompromised = microarray_data %>% filter_at(vars(starts_with("compromised")),all_vars(.==TRUE)) # supprime les lignes avec des valeurs "compromised"
detected = notcompromised %>% filter_at(vars(starts_with("detected")),any_vars(.==TRUE)) # supprime les lignes avec trop de valeurs "not detected"
notdetected = setdiff(notcompromised[[1]], detected[[1]])
fwrite(list(notdetected), notdetected_name)

rownames(detected) = detected[[1]]
rawfiltered = detected[raw_cols]

# Etape 2: Normalisation par quantiles

log2transform = log2(matrix(as.numeric(as.matrix(rawfiltered)), ncol = ncol(rawfiltered)))
quantfiltered = data.frame(normalize.quantiles(log2transform), row.names = rownames(rawfiltered))
if (is.list(new_cols) | is.vector(new_cols)){ colnames(quantfiltered) = new_cols } # renomme les colonnes normalisees

quantfiltered[flag_colnames] = detected[flag_cols]
quantfiltered$GeneSymbol = detected$GeneSymbol
#quantfiltered$EntrezGeneID = detected$EntrezGeneID
#quantfiltered$GeneName = detected$GeneName

write.table(quantfiltered, output_name, sep=';', dec='.', qmethod = "double",  col.names = NA, row.names = TRUE)
