### input parameters

filename = "betacells/betacells_quantile_normalized_new.csv" # file with normalized data
norm_cols = 7:80 # columns containing the normalized data
ctrl_cols = 2:5 # columns containing the control samples
n_conditions = 15 # number of experimental conditions (excluding control)
n_replicas = 5 # replicas per condition
output_name = "betacells/betacells_diffexpr_no5_new"

###

library(limma)
library(EnhancedVolcano)

targets = readTargets(filename, sep=";",row.names = "X") # 
#row.names(targets) = targets$X

for (i in 1:n_conditions){
  if (i == n_conditions){
    exp_cols = norm_cols[(n_replicas*(i-1)+1):(n_replicas*i-2)]
  }
  else{
    exp_cols = norm_cols[(n_replicas*(i-1)+1):(n_replicas*i-1)]
  }
  if (!identical(ctrl_cols, exp_cols)){
    eset = targets[c(ctrl_cols,exp_cols)]
    #exp = rep(0,n_conditions*n_replicas)
    #exp[(n_replicas*(i-1)+1):(n_replicas*i)]=1
    if (i == n_conditions){
      design = cbind("CTRL"=c(1,1,1,1, 0,0,0), "EXP"=c(0,0,0,0, 1,1,1))
    }
    else{
      design = cbind("CTRL"=c(1,1,1,1, 0,0,0,0), "EXP"=c(0,0,0,0, 1,1,1,1))
    }
    
    fit = lmFit(eset, design)
    cont.matrix = makeContrasts(CTRLvsEXP=EXP-CTRL, levels=design)
    fit = contrasts.fit(fit, cont.matrix)
    fit = eBayes(fit)
    fullTable = topTable(fit, adjust="BH", number=dim(eset)[1])
    
    out = paste(output_name,"condition",i,".csv",sep="_")
    write.csv(fullTable, out)
    out = paste(output_name,"volcano",i,".png",sep="_")
    plot = EnhancedVolcano(toptable=fullTable, x='logFC', y='adj.P.Val', lab=targets[rownames(fullTable),"GeneSymbol"], pCutoff=0.1, FCcutoff=1, ylim=c(0,max(-log10(fullTable[,'adj.P.Val']), na.rm=TRUE) + 0.5), DrawConnectors = FALSE,legendPosition = 'none', ylab=bquote(~-Log[10]~italic(adj.P)))
    ggsave(filename=out, plot=plot, device='png', width=15, height=15,unit='cm')
  }
}

